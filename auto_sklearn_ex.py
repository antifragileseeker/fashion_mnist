import autosklearn.classification
import sklearn.cross_validation
import sklearn.datasets
import sklearn.metrics
from utils import mnist_reader
X, y = mnist_reader.load_mnist('data/fashion', kind='train')

X_train, X_test, y_train, y_test = \
    sklearn.cross_validation.train_test_split(X, y, random_state=1)

automl = autosklearn.classification.AutoSklearnClassifier()
automl.fit(X_train, y_train)
y_hat = automl.predict(X_test)
print("Accuracy score", sklearn.metrics.accuracy_score(y_test, y_hat))
