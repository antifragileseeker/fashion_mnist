from utils import mnist_reader
X_train, y_train = mnist_reader.load_mnist('data/fashion', kind='train')
X_test, y_test = mnist_reader.load_mnist('data/fashion', kind='t10k')

from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split

tpot=TPOTClassifier(generations=5, population_size=50, verbosity=2, n_jobs=1)
tpot.fit(X_train, y_train)
print(tpot.score(X_test, y_test))
tpot.export('tpot_fashion_mnist_pipeline.py')
